import re
from spacy.tokens import Doc
from nltk import FreqDist
from nltk.sentiment.vader import SentimentIntensityAnalyzer

class TokenFreqDistComponent:
    """A spaCy v2.0 pipeline component that creates FreqDist objects for the tokens. Additionally,
    ._.token_fd and ._.token_lower_fd are set on the Doc for all tokens and for lowercase tokens respectively."""

    def __init__(self, nlp):

        Doc.set_extension("token_fd", default=None)
        Doc.set_extension("token_lower_fd", default=None)

    def __call__(self, doc):
        doc._.set("token_fd", FreqDist((token.text for token in doc)))
        doc._.set("token_lower_fd", FreqDist((token.lower_ for token in doc)))
        return doc

class WordFreqDistComponent:
    """A spaCy v2.0 pipeline component that creates FreqDist objects for the tokens if they are words. Additionally,
    ._.word_fd and ._.word_lower_fd are set on the Doc for all words and for lowercase words respectively. Depends on TokenFreqDist."""

    def __init__(self, nlp, word_re=re.compile("^'?[a-zA-Z]+'?[a-zA-Z]*$")):
        self.word_re = word_re

        Doc.set_extension("word_fd", default=None)
        Doc.set_extension("word_lower_fd", default=None)

    def __call__(self, doc):
        doc._.set("word_fd", FreqDist(dict((word, freq) for word, freq in doc._.token_fd.items() if self.word_re.match(word))))
        doc._.set("word_lower_fd", FreqDist(dict((word, freq) for word, freq in doc._.token_lower_fd.items() if self.word_re.match(word))))

        return doc

class BigramTrigramFreqDistComponent:
    """A spaCy v2.0 pipeline component that creates FreqDist objects for bigrams and trigrams composed of words (words are converted to lowercase). Additionally,
    ._.bigram_fd and ._.trigram_fd are set on the Doc for all words and for lowercase words respectively. Depends on TokenFreqDist."""

    def __init__(self, nlp, word_re=re.compile("^'?[a-zA-Z]+'?[a-zA-Z]*$")):
        self.word_re = word_re

        Doc.set_extension("bigram_fd", default=None)
        Doc.set_extension("trigram_fd", default=None)

    def __call__(self, doc):
        doc._.set("bigram_fd", FreqDist())
        doc._.set("trigram_fd", FreqDist())

        for sent in doc.sents:
            ngram_spots = []
            for i, token in enumerate(sent):
                if self.word_re.match(token.lower_):
                    ngram_spots.append(token.lower_)
                    if len(ngram_spots) > 3:
                        ngram_spots.pop(0)
                    if len(ngram_spots) == 3:
                        doc._.bigram_fd["_".join(ngram_spots[0:2])] += 1
                        doc._.trigram_fd["_".join(ngram_spots)] += 1
            if len(ngram_spots) > 1:
                doc._.bigram_fd["_".join(ngram_spots[len(ngram_spots) - 2 : len(ngram_spots) + 1])] += 1
        return doc

class VaderSentimentComponent:

    def __init__(self, nlp):
        self.sid = SentimentIntensityAnalyzer()
        Doc.set_extension("polarity_scores", default=None)

    def __call__(self, doc):
        polarity_scores = self.sid.polarity_scores(doc.text)
        doc._.set("polarity_scores", polarity_scores)
        return doc
