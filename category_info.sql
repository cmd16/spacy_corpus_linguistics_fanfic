CREATE VIEW categoryCount AS
(
	SELECT category
		, COUNT(*)
	FROM category c 
	GROUP BY category
	ORDER BY COUNT(*) DESC
) 


CREATE VIEW categoryMarketBasket AS
	WITH cats (baseCat, affCat, Frequency) AS (
	SELECT
		p.category AS baseCat,
		t.category AS affCat,
		COUNT(*) AS Frequency
	FROM
		category p
	JOIN category t ON
		p.work_id = t.work_id
	WHERE
		p.category <> t.category
	GROUP BY
		p.category,
		t.category )
	SELECT
		b.baseCat "Base Category",
		b.catCount AS "# Works With Base",
		cats.affCat "Accompanied By Category",
		cats.Frequency "# Accompanied w/Base",
		CAST (cats.Frequency AS float)
				/CAST(b.catCount as float) "Affinity %"
	FROM cats 
	INNER JOIN
		(SELECT category baseCat, COUNT(*) AS catCount
		FROM category c 
		GROUP BY category 
		) AS b
	ON b.baseCat = cats.baseCat
	ORDER BY 5 DESC 

SELECT * FROM categoryMarketBasket 

SELECT * from categoryCount 

SELECT category
	, COUNT(*)
FROM category c 
JOIN doctorWho 
ON c.work_id = doctorWho.work_id
GROUP BY category
ORDER BY COUNT(*) DESC

SELECT category
	, COUNT(*)
FROM category c 
JOIN hamilton 
ON c.work_id = hamilton.work_id
GROUP BY category
ORDER BY COUNT(*) DESC

SELECT category
	, COUNT(*)
FROM category c 
JOIN lesMis 
ON c.work_id = lesMis.work_id
GROUP BY category
ORDER BY COUNT(*) DESC

SELECT category
	, COUNT(*)
FROM category c 
JOIN sherlock 
ON c.work_id = sherlock.work_id
GROUP BY category
ORDER BY COUNT(*) DESC

SELECT category
	, COUNT(*)
FROM category c 
JOIN tolkien 
ON c.work_id = tolkien.work_id
GROUP BY category
ORDER BY COUNT(*) DESC

SELECT category
	, COUNT(*)
FROM category c 
JOIN undertale 
ON c.work_id = undertale.work_id
GROUP BY category
ORDER BY COUNT(*) DESC
