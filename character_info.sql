CREATE VIEW characterCount AS
	SELECT character
		, COUNT(*)
	FROM character c 
	GROUP BY "character" 
	ORDER BY COUNT(*) DESC 

SELECT work_id
FROM "character" c 
WHERE character IS NULL

CREATE VIEW characterMarketBasket AS
	WITH cats (baseCat, affCat, Frequency) AS (
		SELECT
			p."character" AS baseCat,
			t."character" AS affCat,
			COUNT(*) AS Frequency
		FROM
			character p
		JOIN character t ON
			p.work_id = t.work_id
		WHERE
			p."character" <> t."character" 
		GROUP BY
			p."character" ,
			t."character" 
		HAVING COUNT(*) > 1 
	)
	SELECT
		b.baseCat "Base Character",
		b.catCount AS "# Works With Character",
		cats.affCat "Accompanied By Character",
		cats.Frequency "# Accompanied w/Base",
		CAST (cats.Frequency AS float)
				/CAST(b.catCount as float) "Affinity %"
	FROM cats 
	INNER JOIN
		(SELECT character baseCat, COUNT(*) AS catCount
		FROM character c 
		GROUP BY "character"
		HAVING COUNT(*) >= 50) AS b
	ON b.baseCat = cats.baseCat
	ORDER BY 5 DESC 
