import os
import spacy
from spacy.tokens import DocBin, Doc
import spacy_components
import pandas as pd
import numpy as np

import time
from datetime import timedelta

nlp = spacy.load("en_core_web_sm")
nlp.add_pipe(spacy_components.TokenFreqDistComponent(nlp), last=True)
nlp.add_pipe(spacy_components.WordFreqDistComponent(nlp), last=True)
nlp.add_pipe(spacy_components.BigramTrigramFreqDistComponent(nlp), last=True)
nlp.add_pipe(spacy_components.VaderSentimentComponent(nlp), last=True)

Doc.set_extension("filename", default=None)

nlp.max_length = 9000000

large_files = ["7899862.txt"]  # a 4.8 million word LOTR fanfic

# project_dir = "/Volumes/2TB/Final_Project"
# fanfic_dir = os.path.join(project_dir, "Fanfic_all")
# output_dir = os.path.join(project_dir, "Spacy/docbin")
#
# filenames = [name for name in os.listdir(fanfic_dir) if name.endswith(".txt")]
#
# n = 1000  # How many elements each list should have
# filename_chunks = [filenames[i*n:(i+1)*n] for i in range((len(filenames) + n - 1) // n)]
#
# def file_generator(dir, filenames):
#     for name in filenames:
#         path = os.path.join(dir, name)
#         with open(path, 'r') as file:
#             yield name, file.read()

print("starting")
# 'controversiality': pd.Int64Dtype(),
# df = pd.read_csv('~/reddit-corpus-linguistics/fountainpens-comments_2010_2018-10.csv',
#                  dtype={'edited': 'object', 'name': str, 'body': str, 'downs': np.float64,
#                         'id': str, 'score_hidden': 'object', 'score': pd.Int64Dtype(), 'author': str, 'link_id': str, 'author_flair_text': str,
#                         'parent_id': str, 'archived': 'object', 'distinguished': str, 'subreddit': str, 'created_utc': str,
#                         'subreddit_id': str, 'gilded': pd.Int64Dtype(), 'author_flair_css_class': str, 'retrieved_on': str,
#                         'ups': np.float64, 'removal_reason': str, 'report_reasons': np.float64, 'approved_by': np.float64,
#                         'saved': 'object', 'user_reports': str, 'banned_by': np.float64, 'likes': np.float64, 'body_html': str,
#                         'mod_reports': str, 'created': np.float64, 'num_reports': np.float64, 'replies': str, 'stickied': 'object',
#                         'author_cakeday': 'object', 'can_gild': 'object', 'approved_at_utc': str, 'can_mod_post': 'object',
#                         'collapsed': 'object','collapsed_reason': str, 'banned_at_utc': str, 'is_submitter': 'object', 'permalink': str,
#                         'subreddit_type': str, 'mod_note': np.float64, 'mod_reason_by': np.float64, 'mod_reason_title': np.float64,
#                         'no_follow': 'object', 'send_replies': 'object', 'author_flair_template_id': str, 'author_flair_background_color': str,
#                         'author_flair_richtext': str, 'author_flair_text_color': str, 'author_flair_type': str, 'rte_mode': str,
#                         'author_created_utc': str, 'author_fullname': str, 'subreddit_name_prefixed': str, 'gildings': str,
#                         'author_patreon_flair': str},
#                  parse_dates=['created_utc', 'retrieved_on', 'approved_at_utc', 'author_created_utc'])
start = time.time()
df = pd.read_csv('~/reddit-corpus-linguistics/fountainpens-comments_2010_2018-10_nonewline.csv', parse_dates=['created_utc', 'retrieved_on', 'approved_at_utc', 'author_created_utc'])
delta = timedelta(seconds = time.time() - start)
print("time to read csv:", delta)
start = time.time()
# 8:26:05 to go from 90 to 133 (43). 43/8.5 is about 5 chunks per hour. 102 items left, so 102 * 8.5/43 = 20.162
nlp.max_length = 90000000

# for i in range(len(filename_chunks)):
#     if i >= 59:
#         continue
#     chunk = filename_chunks[i]
#     print(f"chunk {i}: {chunk[0]}-{chunk[-1]}")
#     doc_bin = DocBin(attrs=["TAG", "POS", "LEMMA", "HEAD", "DEP", "ENT_IOB", "ENT_TYPE"], store_user_data=True)
#     try:
#         for name, txt in file_generator(fanfic_dir, chunk):
#             if len(txt) > nlp.max_length:
#                 print(f"{name} is too long")
#                 large_files.append(name)
#                 continue
#             doc = nlp(txt)
#             doc._.set("filename", name)
#             doc_bin.add(doc)
#     except KeyboardInterrupt:
#         print(f"keyboard interrupted, i={i}")
#         break
#     with open(os.path.join(output_dir, f"{chunk[0].replace('.txt', '')}-{chunk[-1].replace('.txt', '')}_docbin"), "wb") as f:
#         f.write(doc_bin.to_bytes())

doc_bin = DocBin(attrs=["TAG", "POS", "LEMMA", "HEAD", "DEP", "ENT_IOB", "ENT_TYPE"], store_user_data=True)
for name, txt in zip(df['id'], df['body']):
    if len(txt) > nlp.max_length:
        print(f"{name} is too long")
        large_files.append(name)
        continue
    doc = nlp(txt)
    doc._.set("filename", name)
    doc_bin.add(doc)
with open(os.path.join("/Users/cat/reddit-corpus-linguistics", "fountainpens_comments_docbin"), "wb") as f:
    f.write(doc_bin.to_bytes())

end = time.time()
delta = timedelta(seconds = end - start)
print("time to do spacy analysis:", delta)
print(f"large files: {large_files}")
