import os
from spacy.tokens import Doc, DocBin
import spacy
from nltk import FreqDist
import spacy_components
import time
from datetime import timedelta
import numpy as np
import math
import json

class Corpus:
    def __init__(self, name, filenames, nlp, in_dir=None, store_sentiment=False):
        self.name = name
        self.filenames = filenames
        self.nlp = nlp
        self.in_dir = in_dir
        self.store_sentiment = store_sentiment

        self.n_docs = 0

        self.noun_chunks_fd = FreqDist()

        self.token_fd = FreqDist()
        self.token_lower_fd = FreqDist()
        self.word_fd = FreqDist()
        self.word_lower_fd = FreqDist()
        self.bigram_fd = FreqDist()
        self.trigram_fd = FreqDist()
        self.stored_attrs = ['token_fd', 'token_lower_fd', 'word_fd', 'bigram_fd', 'trigram_fd']  # the attributes stored in the Doc. TODO: add back in word_lower_fd

        self.person_fd = FreqDist()
        self.norp_fd = FreqDist()
        self.fac_fd = FreqDist()
        self.org_fd = FreqDist()
        self.gpe_fd = FreqDist()
        self.loc_fd = FreqDist()
        self.product_fd = FreqDist()
        self.event_fd = FreqDist()
        self.work_of_art_fd = FreqDist()
        self.law_fd = FreqDist()
        self.language_fd = FreqDist()
        self.ents_list = ["PERSON", "NORP", "FAC", "ORG", "GPE", "LOC", "PRODUCT", "EVENT", "WORK_OF_ART", "LAW", "LANGUAGE"]

        self.adj_fd = FreqDist()
        self.noun_fd = FreqDist()
        self.propn_fd = FreqDist()
        self.part_fd = FreqDist()
        self.adv_fd = FreqDist()
        self.intj_fd = FreqDist()
        self.pos_list = ["ADJ", "NOUN", "PROPN", "PART", "ADV", "INTJ"]

        self.vb_fd = FreqDist()
        self.vbd_fd = FreqDist()
        self.vbg_fd = FreqDist()
        self.vbn_fd = FreqDist()
        self.vbp_fd = FreqDist()
        self.vbz_fd = FreqDist()
        self.tag_list = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]

        self.acl_fd = FreqDist()
        self.acomp_fd = FreqDist()
        self.advcl_fd = FreqDist()
        self.advmod_fd = FreqDist()
        self.agent_fd = FreqDist()
        self.amod_fd = FreqDist()
        self.appos_fd = FreqDist()
        self.attr_fd = FreqDist()
        self.aux_fd = FreqDist()
        self.auxpass_fd = FreqDist()
        self.case_fd = FreqDist()
        self.cc_fd = FreqDist()
        self.ccomp_fd = FreqDist()
        self.compound_fd = FreqDist()
        self.conj_fd = FreqDist()
        self.csubj_fd = FreqDist()
        self.csubjpass_fd = FreqDist()
        self.dative_fd = FreqDist()
        self.dep_fd = FreqDist()
        self.det_fd = FreqDist()
        self.dobj_fd = FreqDist()
        self.expl_fd = FreqDist()
        self.mark_fd = FreqDist()
        self.meta_fd = FreqDist()
        self.neg_fd = FreqDist()
        self.nmod_fd = FreqDist()
        self.npadvmod_fd = FreqDist()
        self.nsubj_fd = FreqDist()
        self.nsubjpass_fd = FreqDist()
        self.nummod_fd = FreqDist()
        self.oprd_fd = FreqDist()
        self.parataxis_fd = FreqDist()
        self.pcomp_fd = FreqDist()
        self.pobj_fd = FreqDist()
        self.poss_fd = FreqDist()
        self.preconj_fd = FreqDist()
        self.predet_fd = FreqDist()
        self.prep_fd = FreqDist()
        self.prt_fd = FreqDist()
        self.punct_fd = FreqDist()
        self.quantmod_fd = FreqDist()
        self.relcl_fd = FreqDist()
        self.root_fd = FreqDist()
        self.xcomp_fd = FreqDist()
        self.dep_list = ['ROOT', 'acl', 'acomp', 'advcl', 'advmod', 'agent', 'amod', 'appos', 'attr', 'aux', 'auxpass',
                         'case', 'cc', 'ccomp', 'compound', 'conj', 'csubj', 'csubjpass', 'dative', 'dep', 'det', 'dobj',
                         'expl', 'mark', 'meta', 'neg', 'nmod', 'npadvmod', 'nsubj', 'nsubjpass', 'nummod', 'oprd',
                         'parataxis', 'pcomp', 'pobj', 'poss', 'preconj', 'predet', 'prep', 'prt', 'punct', 'quantmod',
                         'relcl', 'xcomp']

        self.positive_array = []
        self.negative_array = []
        self.neutral_array = []
        self.compound_array = []

    def get_n_words(self):
        return self.word_fd.N()

    def read_corpus(self, in_dir=None, dep_only=False, sentiment_only=False):
        if in_dir is None:
            in_dir = self.in_dir
        if in_dir is None:
            in_dir = "."

        for docbin_name in sorted(os.listdir(in_dir)):
            if not docbin_name.endswith("docbin"):
                continue
            print(f"{self.name} at {docbin_name}")
            with open(os.path.join(in_dir, docbin_name), "rb") as f:
                doc_bytes = f.read()
            docbin = DocBin(store_user_data=True).from_bytes(doc_bytes)
            for doc in docbin.get_docs(self.nlp.vocab):
                if doc._.filename not in self.filenames:
                    continue
                self.n_docs += 1

                if dep_only:
                    for token in doc:
                        if token.dep_ in self.dep_list:
                            getattr(self, token.dep_.lower() + "_fd")[token.lower_] += 1
                    continue

                if self.store_sentiment:
                    self.positive_array.append(doc._.polarity_scores['pos'])
                    self.negative_array.append(doc._.polarity_scores['neg'])
                    self.neutral_array.append(doc._.polarity_scores['neu'])
                    self.compound_array.append(doc._.polarity_scores['compound'])
                    if sentiment_only:
                        continue

                self.noun_chunks_fd.update(str(x) for x in doc.noun_chunks if len(x) > 1)

                for attr in self.stored_attrs:
                    getattr(self, attr).update(doc._.get(attr))

                ents = doc.ents
                for ent_type in self.ents_list:
                    getattr(self, ent_type.lower() + "_fd").update((ent.text for ent in ents if ent.label_ == ent_type))

                for token in doc:
                    if token.pos_ in self.pos_list:
                        getattr(self, token.pos_.lower() + "_fd")[token.lower_] += 1
                    elif token.tag_ in self.tag_list:
                        getattr(self, token.tag_.lower() + "_fd")[token.lower_] += 1
                    if token.dep_ in self.dep_list:
                        getattr(self, token.dep_.lower() + "_fd")[token.lower_] += 1

    @staticmethod
    def freqdist_to_wordlistfile(freqdist, filename):
        """
        Store the results of a frequency distribution in a txt file in a format similar to the one generated by AntConc

        :param freqdist: the FreqDist object
        :param filename: the txt file to store the results in. Must be a filename.
        :return:
        """
        tokens = freqdist.N()
        types = len(freqdist)
        # print(filename, tokens, types)
        with open(filename, 'w') as f_out:
            f_out.write(f"#Word types: {types}\n")  # unique words
            f_out.write(f"#Word tokens: {tokens}\n")  # total words
            f_out.write(f"#Search results: 0\n")  # kept so this can be opened by Antconc
            rank = 1
            for tup in freqdist.most_common():
                word = tup[0]
                freq = str(tup[1])
                f_out.write(f"{rank}\t{freq}\t{word}\t\n")
                rank += 1

    @staticmethod
    def wordlist_to_freqdist(wordlist_file):
        """
        Given a wordlist return a frequency distribution as well as the number of types and tokens
        :param wordlist_file: a .txt file generated from AntConc or freqdist_to_wordlistfile. Line 1 has the number of types,
        line 2 the number of tokens, and lines 4-end are in the format "rank\tword\tfrequency". Can be a filename or an open file object.
        :return: a frequency distribution (word frequencies), the number of types, and the number of tokens
        """
        try:
            corpus = open(wordlist_file)
            close = True
        except TypeError:
            corpus = wordlist_file
            close = False
        types = next(corpus)
        types = int(types[types.index("types") + 7:])
        tokens = next(corpus)
        tokens = int(tokens[tokens.index("tokens") + 8:])
        next(corpus)
        # load corpus into a FreqDist
        freqdist1 = FreqDist()
        for line in corpus:
            try:
                rank, freq, word = line.strip().split('\t')
            except ValueError as e:
                # print('error with line:', line)
                raise e
            freqdist1[word] = int(freq)  # store the word and its frequency
        if close:
            corpus.close()
        return freqdist1

    def word_fd_to_lower(self, in_dir):
        if len(self.word_fd) == 0:
            self.read_corpus_stats(in_dir, include_attr=['word_fd'])
        for key in self.word_fd:
            self.word_lower_fd[key.lower()] += self.word_fd[key]

    def write_corpus_stats(self, out_dir, include_attr=None, exclude_attr=None, include_groups=None):
        iterator = self.__dict__
        if include_attr:
            iterator = include_attr
        elif exclude_attr:
            iterator = [x for x in iterator if x not in exclude_attr]
        elif include_groups:
            iterator = []
            for group in include_groups:
                for item in getattr(self, group):
                    if not item.endswith("fd"):
                        item = item.lower() + "_fd"
                    iterator.append(item)
        elif self.store_sentiment:
            iterator = list(iterator)
            iterator.append('sentiment')
        for attr in iterator:
            if attr.endswith("fd"):
                out_name = os.path.join(os.path.join(out_dir, attr), self.name)
                self.freqdist_to_wordlistfile(getattr(self, attr), out_name)
        if self.store_sentiment and 'sentiment' in iterator:
            out_name = os.path.join(os.path.join(out_dir, "polarity"), self.name)
            with open(out_name, 'w') as f_out:
                f_out.write(f"average compound: {np.mean(self.compound_array)}\n")
                f_out.write(f"max compound: {max(self.compound_array)}\n")
                f_out.write(f"min compound: {min(self.compound_array)}\n")
                f_out.write(f"average positive: {np.mean(self.positive_array)}\n")
                f_out.write(f"max positive: {max(self.positive_array)}\n")
                f_out.write(f"min positive: {min(self.positive_array)}\n")
                f_out.write(f"average negative: {np.mean(self.negative_array)}\n")
                f_out.write(f"max negative: {max(self.negative_array)}\n")
                f_out.write(f"min negative: {min(self.negative_array)}\n")

    def read_corpus_stats(self, in_dir, include_attr=None, exclude_attr=None):
        iterator = self.__dict__
        if include_attr:
            iterator = include_attr
        elif exclude_attr:
            iterator = [x for x in iterator if x not in exclude_attr]
        for attr in iterator:
            if attr.endswith("fd"):
                in_name = os.path.join(os.path.join(in_dir, attr), self.name)
                try:
                    setattr(self, attr, self.wordlist_to_freqdist(in_name))
                except ValueError as e:
                    pass
                    # print(f"error reading {attr}: {e}")

    def write_keywords(self, corpus2, out_dir, p=0.01, include_attr=None, exclude_attr=None, include_groups=None, name=None):
        iterator = self.__dict__
        if include_attr:
            iterator = include_attr
        elif exclude_attr:
            iterator = [x for x in iterator if x not in exclude_attr]
        elif include_groups:
            iterator = []
            for group in include_groups:
                for item in getattr(self, group):
                    if not item.endswith("fd"):
                        item = item.lower() + "_fd"
                    iterator.append(item)
        elif self.store_sentiment:
            iterator = list(iterator)
            iterator.append('sentiment')
        for attr in iterator:
            if attr.endswith("fd"):
                keyword_dict = Corpus.keyword_analysis(getattr(self, attr), getattr(corpus2, attr), p)
                if not keyword_dict:
                    print("no keywords found")
                    continue
                if name is None:
                    name = self.name.replace('.txt', '.json')
                out_name = os.path.join(os.path.join(out_dir, attr), name)
                with open(out_name, 'w') as f:
                    f.write(json.dumps(keyword_dict))

    @staticmethod
    def keyword_analysis(fd1, fd2, p=0.01):
        """
        Keyword analysis from wordlist files.
        Limitations: the numbers are a little bit different here (about 10 lower or higher in the few examples I looked at)
        from the loglikelihood calculator at http://ucrel.lancs.ac.uk/cgi-bin/llsimple.pl?f1=3852&f2=2179&t1=3802120&t2=3569518
        even though I used the same formula. Probably due to different floating point arithmetic.
        Note: values are only stored if keyness is statistically significant as determined by the p value and
        normalized frequency 1 > normalized frequency 2
        :param fd1: name of the first FreqDist.
        :param fd2: name of the second FreqDist.
        :param p: p value (threshold for statistical significance)
        :return: a dictionary containing the words, keyness, raw frequencies and normalized frequencies.
        """
        if p == 0.5:
            crit = 3.84
        elif p == 0.01:
            crit = 6.63
        elif p == 0.001:
            crit = 10.83
        elif p == 0.0001:
            crit = 15.13
        elif p == 0:
            crit = 0
        else:
            print(p,
                  "not recognized as a valid p value (options are .5, .01, .001, .0001, and 0 for all results). Setting p value to .01")
            crit = 6.63  # the default is 1% error margin
        types1 = len(fd1)
        tokens1 = fd1.N()
        if tokens1 == 0:
            # print("freqdist 1 is empty")
            return False  # the wordlist is empty or something else went wrong
        types2, tokens2 = len(fd2), fd2.N()
        if tokens2 == 0:
            # print("freqdist 2 is empty")
            return False  # the wordlist is empty or something else went wrong
        keyword_dict = {}

        for key in fd1:
            freq1 = fd1[key]
            freq2 = fd2[key]
            keyness, norm1, norm2 = Corpus.calculate_keyness(freq1, freq2, tokens1, tokens2)
            if keyness >= crit:
                keyword_dict[key] = {'keyness': keyness, 'freq1': freq1, 'norm1': norm1, 'freq2': freq2, 'norm2': norm2}
        return keyword_dict

    @staticmethod
    def calculate_keyness(freq1, freq2, tokens1, tokens2):
        """
        Given both frequencies and the number of tokens, return the keyness as well as the normalized frequencies
        :param freq1: frequency of word in corpus1
        :param freq2: frequency of word in corpus2
        :param tokens1: number of tokens in corpus1
        :param tokens2: number of tokens in corpus2
        :return: keyness, normalized frequency 1, normalized frequency 2
        """
        num = (freq1 + freq2) / (tokens1 + tokens2)
        E1 = tokens1 * num
        E2 = tokens2 * num
        try:
            keyness = 2 * (freq1 * math.log(freq1 / E1) + (freq2 * math.log(freq2 / E2)))
        except ValueError:
            keyness = 2 * (freq1 * math.log(freq1 / E1))  # the second part equals 0
        norm1 = Corpus.normalize_count_permillion(freq1, tokens1)
        norm2 = Corpus.normalize_count_permillion(freq2, tokens2)
        return keyness, norm1, norm2

    @staticmethod
    def normalize_count_permillion(freq, size):
        """
        Normalize a frequency given the frequency and the size
        :param freq: frequency
        :param size: the size of the data (e.g., number of words in the corpus)
        :return: frequency per million
        """
        return freq * 1000000 / size

    def filter_bigrams(self, func):
        for bigram in self.bigram_fd:
            if func(bigram):
                yield bigram

    def filter_bigrams_options(self, **kwargs):
        min_freq = kwargs.get("min_freq")
        first_filter = kwargs.get("first")
        second_filter = kwargs.get("second")
        either_filter = kwargs.get("either")

        for bigram in self.bigram_fd:
            if min_freq and self.bigram_fd[bigram] < min_freq:
                continue
            first, second = bigram.split("_")
            if first_filter and not first_filter.match(first):
                continue
            if second_filter and not second_filter.match(second):
                continue
            if either_filter:
                if either_filter.match(first) or either_filter.match(second):
                    pass
                else:
                    continue
            yield bigram

    def print_bigrams_options(self, **kwargs):
        for bigram in self.filter_bigrams_options(**kwargs):
            print(bigram, self.bigram_fd[bigram], f"{self.bigram_fd.freq(bigram):.8f}")

    def sum_filter_bigrams(self, func):
        return sum(self.bigram_fd[bigram] for bigram in self.bigram_fd if func(bigram))

    def filter_trigrams_options(self, **kwargs):
        for trigram in self.trigram_fd:
            min_freq = kwargs.get("min_freq")
            if min_freq and self.trigram_fd[trigram] < min_freq:
                continue
            first, second, third = trigram.split("_")
            first_filter = kwargs.get("first")
            if first_filter and not first_filter.match(first):
                continue
            second_filter = kwargs.get("second")
            if second_filter and not second_filter.match(second):
                continue
            third_filter = kwargs.get("third")
            if third_filter and not third_filter.match(third):
                continue
            either_filter = kwargs.get("either")
            if either_filter:
                if either_filter.match(first) or either_filter.match(second) or either_filter.match(third):
                    pass
                else:
                    continue
            yield trigram

    def print_trigrams_options(self, **kwargs):
        for trigram in self.filter_trigrams_options(**kwargs):
            print(trigram, self.trigram_fd[trigram], f"{self.trigram_fd.freq(trigram):.8f}")

    def filter_trigrams(self, func):
        for trigram in self.trigram_fd:
            if func(trigram):
                yield trigram

    def sum_filter_trigrams(self, func):
        return sum(self.trigram_fd[trigram] for trigram in self.trigram_fd if func(trigram))

    def create_bigram_contingency(self, bigram):
        word1, word2 = bigram.split("_")
        result = dict()
        result['o11'] = self.bigram_fd[bigram]
        result['r1'] = self.sum_filter_bigrams(lambda x: x.startswith(f"{word1}_"))
        result['o12'] = result['r1'] - result['o11']
        result['r2'] = self.bigram_fd.N() - result['r1']
        result['c1'] = self.sum_filter_bigrams(lambda x: x.endswith(f"_{word2}"))
        result['o21'] = result['c1'] - result['o11']
        result['c2'] = self.bigram_fd.N() - result['c1']
        result['o22'] = result['c2'] - result['o12']

        result['e11'] = (result['r1'] * result['c1']) / self.bigram_fd.N()
        result['e12'] = (result['r1'] * result['c2']) / self.bigram_fd.N()
        result['e21'] = (result['r2'] * result['c1']) / self.bigram_fd.N()
        result['e22'] = (result['r2'] * result['c2']) / self.bigram_fd.N()

        return result

    def chisq(self, o11, o22, o12, o21, r1, r2, c1, c2):
        """
        bigrams only

        :param o11:
        :param o22:
        :param o12:
        :param o21:
        :param r1:
        :param r2:
        :param c1:
        :param c2:
        :return:
        """
        return (self.bigram_fd.N() * (abs(o11 * o22 - o12 * o21) - self.bigram_fd.N()/2)**2) / (r1 * r2 * c1 * c2)

    def odds_ratio(self, o11, o22, o12, o21):
        """
        bigrams only

        :param o11:
        :param o22:
        :param o12:
        :param o21:
        :return:
        """
        return math.log( ((o11 + 0.5)*(o22 + 0.5)) / ((o12 + 0.5)*(o21 + 0.5)) )

    def ll(self, o, e):
        """
        bigrams and trigrams

        :param o:
        :param e:
        :return:
        """
        return 2 * sum(ox * math.log(ox/ex) for ox, ex in zip(o, e) if ox != 0)

    def pmi(self, o11, e11):
        """
        bigrams and trigrams

        :param o11:
        :param e11:
        :return:
        """
        return math.log(o11 / e11)

    def calculate_bigrams(self, min_freq=5):
        result = []
        for bigram, freq in self.bigram_fd.most_common():
            if freq < min_freq:
                break

            contingency = self.create_bigram_contingency(bigram)

            chisq = self.chisq(o11=contingency['o11'], o22=contingency['o22'], o12=contingency['o12'], o21=contingency['o21'],
                               r1=contingency['r1'], r2=contingency['r2'], c1=contingency['c1'], c2=contingency['c2'])
            odds_ratio = self.odds_ratio(o11=contingency['o11'], o12=contingency['o12'], o21=contingency['o21'], o22=contingency['o22'])

            pmi = self.pmi(contingency['o11'], contingency['e11'])

            o = []
            e = []
            for a in '12':
                for b in '12':
                    key = a + b
                    o.append(contingency[f"o{key}"])
                    e.append(contingency[f"e{key}"])
            ll = self.ll(o, e)
            result.append({'bigram': bigram, 'freq': freq, 'pmi': pmi, 'll': ll, 'chisq': chisq, 'odds_ratio': odds_ratio})
        return result

    def create_trigram_contingency(self, trigram):
        word1, word2, word3 = trigram.split("_")

        result = dict()

        result['o111'] = self.trigram_fd[trigram]
        result['o112'] = self.sum_filter_trigrams(lambda x: x.startswith(f"{word1}_{word2}_") and not x.endswith(f"{word3}"))
        result['o121'] = self.sum_filter_trigrams(lambda x: x.startswith(f"{word1}_") and f"_{word2}_" not in x and x.endswith(f"_{word3}"))
        result['o122'] = self.sum_filter_trigrams(lambda x: x.startswith(f"{word1}_") and f"_{word2}_" not in x and not x.endswith(f"_{word3}") )
        result['o211'] = self.sum_filter_trigrams(lambda x: not x.startswith(f"{word1}_") and x.endswith(f"_{word2}_{word3}"))
        result['o212'] = self.sum_filter_trigrams(lambda x: not x.startswith(f"{word1}_") and f"_{word2}_" and not x.endswith(f"_{word3}"))
        result['o221'] = self.sum_filter_trigrams(lambda x: not x.startswith(f"{word1}_") and f"_{word2}_" and x.endswith(f"_{word3}"))
        result['o222'] = self.sum_filter_trigrams(lambda x: not x.startswith(f"{word1}_") and f"_{word2}_" and not x.endswith(f"_{word3}"))

        result['o1pp'] = result['o111'] + result['o112'] + result['o121'] + result['o122']
        result['op1p'] = result['o111'] + result['o112'] + result['o211'] + result['o212']
        result['opp1'] = result['o111'] + result['o121'] + result['o211'] + result['o221']
        result['o2pp'] = result['o211'] + result['o212'] + result['o221'] + result['o222']
        result['op2p'] = result['o121'] + result['o122'] + result['o221'] + result['o222']
        result['opp2'] = result['o112'] + result['o122'] + result['o212'] + result['o222']

        denom = self.trigram_fd.N()**2

        result['e111'] = (result['o1pp'] * result['op1p'] * result['opp1'])/denom
        result['e112'] = (result['o1pp'] * result['op1p'] * result['opp2'])/denom
        result['e122'] = (result['o1pp'] * result['op2p'] * result['opp2'])/denom
        result['e222'] = (result['o2pp'] * result['op2p'] * result['opp2'])/denom
        result['e121'] = (result['o1pp'] * result['op2p'] * result['opp1'])/denom
        result['e212'] = (result['o2pp'] * result['op1p'] * result['opp2'])/denom
        result['e211'] = (result['o2pp'] * result['op1p'] * result['opp1'])/denom
        result['e221'] = (result['o2pp'] * result['op2p'] * result['opp1'])/denom

        return result

    def calculate_trigrams(self, min_freq=5):
        result = []
        for trigram, freq in self.trigram_fd.most_common():
            if freq < min_freq:
                break

            contingency = self.create_trigram_contingency(trigram)
            pmi = self.pmi(contingency['o111'], contingency['e111'])
            o = []
            e = []
            for a in '12':
                for b in '12':
                    for c in '12':
                        key = a + b + c
                        o.append(contingency[f"o{key}"])
                        e.append(contingency[f"e{key}"])
            ll = self.ll(o, e)
            result.append({'trigram': trigram, 'freq': freq, 'pmi': pmi, 'll': ll})

        return result

    def write_bigram_stats(self, out_dir):
        with open(os.path.join(out_dir, self.name.replace('.txt', '.json')), 'w') as f:
            result = self.calculate_bigrams()
            f.write(json.dumps(result))

    def write_trigram_stats(self, out_dir):
        with open(os.path.join(out_dir, self.name.replace('.txt', '.json')), 'w') as f:
            result = self.calculate_trigrams()
            f.write(json.dumps(result))

    def print_freqs_options(self, tokens, fd_name):
        return Corpus.print_freqs(tokens, getattr(self, fd_name))

    def print_group_freqs_options(self, groups, fd_name):
        return Corpus.print_group_freqs(groups, getattr(self, fd_name))

    @staticmethod
    def print_freqs(tokens, fd):
        for token in tokens:
            freq = fd[token]
            norm = Corpus.normalize_count_permillion(fd[token],fd.N())
            rank = Corpus.get_rank(token, fd)
            print(f"{token:<20}: {freq:<7} ({norm:.2f}) #{rank:>20}")

    @staticmethod
    def print_group_freqs(groups, fd):
        for group in groups:
            freq = sum(fd[token] for token in group)
            norm = Corpus.normalize_count_permillion(freq, fd.N())
            print(f"{group[0]:<20}: {freq:<7} ({norm:.2f})")
        return freq, norm

    @staticmethod
    def get_rank(token, fd):
        i = 1
        for t, freq in fd.most_common():
            if t == token:
                break
            i += 1
        else:
            i = -1
        return i


if __name__ == "__main__":
    project_dir = "/Users/cat/Final_project"
    corpus_name_dir = os.path.join(project_dir, "Fanfic lists")
    fanfic_dir = os.path.join(project_dir, "Fanfic_all")
    corpus_stats_dir = os.path.join(project_dir, "Spacy")
    docbin_dir = os.path.join(corpus_stats_dir, "docbin")

    nlp = spacy.load("en_core_web_sm")
    nlp.add_pipe(spacy_components.TokenFreqDistComponent(nlp), last=True)
    nlp.add_pipe(spacy_components.WordFreqDistComponent(nlp), last=True)
    nlp.add_pipe(spacy_components.BigramTrigramFreqDistComponent(nlp), last=True)
    nlp.add_pipe(spacy_components.VaderSentimentComponent(nlp), last=True)

    Doc.set_extension("filename", default=None)

    nlp.max_length = 90000000

    completed_corpora = ["BBC Sherlock.txt", "Classic Who.txt", "Doctor Who 1-100.txt", "Doctor Who 1-1000.txt",
                         "Doctor Who 100001-1000000.txt", "Doctor Who 10001-100000.txt", "Doctor Who 100001-500000.txt",
                         "Doctor Who 10001-50000.txt", "Doctor Who 1001-10000.txt", "Doctor Who 1001-5000.txt",
                         "Doctor Who 2009.txt", "Doctor Who 2010.txt", "Doctor Who 2011.txt", "Doctor Who 2012.txt",
                         "Doctor Who 2013.txt", "Doctor Who 2014.txt", "Doctor Who 2015.txt", "Doctor Who 2016.txt",
                         "Doctor Who 2017.txt", "Doctor Who 2018.txt", "Doctor Who 50001-100000.txt", "Doctor Who 5001-10000.txt",
                         "Doctor Who 500001-1000000.txt", "Doctor Who Angst.txt", "Doctor Who AU.txt", "Doctor Who Completed.txt",
                         "Doctor Who Crack.txt", "Doctor Who Established.txt",
                         "Doctor Who Explicit.txt",
                         "Doctor Who FF.txt", "Doctor Who Fluff.txt", "Doctor Who FM.txt", "Doctor Who Friendship.txt",
                         "Doctor Who Humor.txt", "Doctor Who Comfort.txt", "Doctor Who MM.txt", "Doctor Who Multi.txt",
                         "Doctor Who Not Rated.txt", "Doctor Who Romance.txt", "Hamilton 1-100.txt", "Hamilton 1-1000.txt",
                         "Hamilton 100001-1000000.txt", "Star Trek Friendship.txt"]

    # for corpus_name in completed_corpora:
    #     print(corpus_name)
    #     start = time.time()
    #     with open(os.path.join(corpus_name_dir, corpus_name), 'r') as corpus_name_file:
    #         in_names = [f"{x.strip()}.txt" for x in corpus_name_file.readlines()]
    #     corpus = Corpus(corpus_name, in_names, nlp, in_dir=docbin_dir)
    #     corpus.read_corpus(dep_only=True)
    #     corpus.write_corpus_stats(corpus_stats_dir, include_groups=["dep_list"])
    #     print(f"calculated and wrote dependency info for {corpus_name}, time was {timedelta(seconds=time.time() - start)}")

    for corpus_name in os.listdir(corpus_name_dir):
        if corpus_name.startswith("."):
            continue
        if corpus_name in completed_corpora:
            continue
        if "all" in corpus_name:
            print(f"skipping corpus with 'all': {corpus_name}")
            continue
        start = time.time()
        print(corpus_name)
        in_names = []
        with open(os.path.join(corpus_name_dir, corpus_name), 'r') as corpus_name_file:
            in_names = sorted([f"{x.strip()}.txt" for x in corpus_name_file.readlines()])
        corpus = Corpus(corpus_name, in_names, nlp, in_dir=docbin_dir)
        corpus.read_corpus()
        corpus.write_corpus_stats(corpus_stats_dir)
        end = time.time()
        delta = timedelta(seconds=end - start)
        print(f"elapsed time for {corpus_name} with {corpus.n_docs} docs and {corpus.get_n_words()} words total: {delta}")

    # start = time.time()
    # corpus_name = "fountainpens_comments.txt"
    # in_names = list(pd.read_csv('/Users/cat/reddit-corpus-linguistics/fountainpens-comments_2010_2018-10_nonewline.csv')['id'])
    # corpus = Corpus(corpus_name, in_names, nlp, in_dir='/Users/cat/reddit-corpus-linguistics', store_sentiment=True)
    # corpus.read_corpus(sentiment_only=True)
    # corpus.write_corpus_stats(os.path.join('/Users/cat/reddit-corpus-linguistics', 'stats'), include_attr=['sentiment'])
    # end = time.time()
    # delta = timedelta(seconds=end - start)
    # print(f"elapsed time for {corpus_name} with {corpus.n_docs} docs and {corpus.get_n_words()} words total (sentiment only): {delta}")

# Doctor Who 1-1000.txt at 337488-3409460_docbin
# elapsed time for Doctor Who 1-1000.txt with 21467 docs and 10766776 words total: 13:54:29.391329
# elapsed time for Doctor Who 100001-1000000.txt with 249 docs and 40579296 words total: 18:41:44.247075
# elapsed time for Doctor Who 10001-100000.txt with 5592 docs and 157134634 words total: 9:10:27.426671
# elapsed time for Doctor Who 100001-500000.txt with 245 docs and 38715853 words total: 8:49:53.633438
# elapsed time for Doctor Who 10001-50000.txt with 4898 docs and 108691474 words total: 8:45:02.386876
# elapsed time for Doctor Who 1001-10000.txt with 29101 docs and 89290390 words total: 9:33:46.669498
# elapsed time for Doctor Who 1001-5000.txt with 24586 docs and 56649467 words total: 8:02:58.773053
# elapsed time for Doctor Who 2009.txt with 1813 docs and 6007030 words total: 12:21:44.548988
# elapsed time for Doctor Who 2010.txt with 3212 docs and 12254357 words total: 8:49:29.165085
# elapsed time for Doctor Who 2011.txt with 4030 docs and 13460570 words total: 9:59:29.228190
# elapsed time for Doctor Who 2012.txt with 6207 docs and 28594465 words total: 8:39:20.043393
# elapsed time for Doctor Who 2013.txt with 8441 docs and 42609886 words total: 8:47:29.793603
# elapsed time for Doctor Who 2014.txt with 8295 docs and 48368697 words total: 8:45:05.967071
# elapsed time for Doctor Who 2015.txt with 7667 docs and 51197954 words total: 9:26:47.159042
# elapsed time for Doctor Who 2016.txt with 7227 docs and 44601893 words total: 9:53:38.956860
# elapsed time for Doctor Who 2017.txt with 6997 docs and 43035239 words total: 8:39:11.604613
# elapsed time for Doctor Who 2018.txt with 255 docs and 935821 words total: 9:48:49.110284
# elapsed time for Doctor Who 50001-100000.txt with 694 docs and 48443160 words total: 8:46:42.802225
# elapsed time for Doctor Who 5001-10000.txt with 4515 docs and 32640923 words total: 10:11:08.618954
# elapsed time for Doctor Who Angst.txt with 6911 docs and 54693554 words total: 18:07:29.416112
# elapsed time for Doctor Who AU.txt with 6805 docs and 75073811 words total: 10:58:38.601454
# elapsed time for Doctor Who Completed.txt with 51052 docs and 228301277 words total: 13:09:34.287663
# elapsed time for Doctor Who Crack.txt with 1345 docs and 5058989 words total: 9:28:57.310675
# elapsed time for Doctor Who Established.txt with 682 docs and 2989611 words total: 9:59:00.049811
# elapsed time for Doctor Who Explicit.txt with 5179 docs and 49466036 words total: 11:01:41.005482
# elapsed time for Doctor Who FF.txt with 1914 docs and 6474729 words total: 11:53:26.464963
# elapsed time for Doctor Who Fluff.txt with 7313 docs and 33193834 words total: 11:32:38.522356
# elapsed time for Doctor Who FM.txt with 18628 docs and 106942631 words total: 13:04:51.971491
# elapsed time for Doctor Who Friendship.txt with 1914 docs and 13078191 words total: 11:37:17.274077
# elapsed time for Doctor Who Gen.txt with 13755 docs and 44690384 words total: 9:35:39.341293
# elapsed time for Doctor Who General Audiences.txt with 25022 docs and 60051888 words total: 9:40:55.350264
# elapsed time for Doctor Who Humor.txt with 3560 docs and 19331711 words total: 11:42:20.813726
# elapsed time for Doctor Who Hurt Comfort.txt with 3125 docs and 27456344 words total: 9:31:42.793351
# elapsed time for Doctor Who Mature.txt with 5827 docs and 61065380 words total: 13:15:35.030061
# elapsed time for Doctor Who MM.txt with 9468 docs and 44827082 words total: 10:26:16.037322
# elapsed time for Doctor Who Multi.txt with 1351 docs and 9700759 words total: 15:41:52.395069
# elapsed time for Doctor Who Not Rated.txt with 3641 docs and 14854386 words total: 12:06:59.840521
# elapsed time for Doctor Who Other.txt with 677 docs and 1953910 words total: 1 day, 0:33:33.401274
# elapsed time for Doctor Who Romance.txt with 4733 docs and 45641118 words total: 12:44:33.031672
# elapsed time for Doctor Who Teen And Up Audiences.txt with 16740 docs and 112333406 words total: 9:09:51.254197
# elapsed time for Doctor Who Updated.txt with 5357 docs and 69469819 words total: 15:13:46.984340
# elapsed time for Hamilton 1-100.txt with 108 docs and 5067 words total: 12:38:37.086814
# elapsed time for Hamilton 1-1000.txt with 3671 docs and 2216039 words total: 11:17:57.111763
# elapsed time for Hamilton 100001-1000000.txt with 39 docs and 7072210 words total: 9:17:48.485498
# elapsed time for Hamilton 100001-500000.txt with 38 docs and 6378647 words total: 10:06:40.929263
# elapsed time for Star Trek Friendship.txt with 2212 docs and 21840088 words total: 7 days, 9:44:02.662771
