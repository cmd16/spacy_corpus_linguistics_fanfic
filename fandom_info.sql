CREATE VIEW fandomCount AS
	SELECT fandom
		, COUNT(*)
	FROM fandom
	GROUP BY "fandom" 
	ORDER BY COUNT(*) DESC 

CREATE VIEW fandomMarketBasket AS
	WITH cats (baseCat, affCat, Frequency) AS (
		SELECT
			p."fandom" AS baseCat,
			t."fandom" AS affCat,
			COUNT(*) AS Frequency
		FROM
			fandom p
		JOIN fandom t ON
			p.work_id = t.work_id
		WHERE
			p."fandom" <> t."fandom" 
		GROUP BY
			p."fandom" ,
			t."fandom" 
		HAVING COUNT(*) > 1 
	)
	SELECT
		b.baseCat "Base Fandom",
		b.catCount AS "# Works With Fandom",
		cats.affCat "Accompanied By Fandom",
		cats.Frequency "# Accompanied w/Base",
		CAST (cats.Frequency AS float)
				/CAST(b.catCount as float) "Affinity %"
	FROM cats 
	INNER JOIN
		(SELECT fandom baseCat, COUNT(*) AS catCount
		FROM fandom c 
		GROUP BY "character"
		HAVING COUNT(*) >= 50) AS b
	ON b.baseCat = cats.baseCat
	ORDER BY 5 DESC 

	
CREATE VIEW doctorWho AS
	SELECT DISTINCT work_id
	FROM fandom f2 
	WHERE fandom LIKE 'Doctor Who%'
		OR fandom IN ('Torchwood', 'Sarah Jane Adventures')

SELECT 'Doctor Who', COUNT(DISTINCT work_id) 
FROM doctorWho

CREATE VIEW hamilton AS
	SELECT DISTINCT work_id
	FROM fandom f2 
	WHERE fandom LIKE 'Hamilton%'

SELECT 'Hamilton', COUNT(DISTINCT work_id) 
FROM hamilton

CREATE VIEW lesMis AS
	SELECT DISTINCT work_id 
	FROM fandom f2 
	WHERE fandom LIKE 'Les Mis%'

SELECT 'Les Mis', COUNT(DISTINCT work_id) 
FROM lesMis

CREATE VIEW sherlock AS
	SELECT DISTINCT work_id 
	FROM fandom f2 
	WHERE fandom LIKE 'Sherlock%'
		OR fandom IN ('BBC Sherlock', 'Elementary')

SELECT 'Sherlock', COUNT(DISTINCT work_id) 
FROM sherlock

DROP VIEW tolkien

CREATE VIEW tolkien AS
	SELECT DISTINCT work_id 
	FROM fandom f
	WHERE fandom LIKE '%Tolkien%'
		OR fandom LIKE '%Lord of the Rings%'
		OR fandom LIKE '%Hobbit%'

SELECT 'Tolkien', COUNT(DISTINCT work_id) 
FROM tolkien 
		
CREATE VIEW undertale AS
	SELECT DISTINCT work_id 
	FROM fandom f
	WHERE fandom = 'Undertale'

SELECT 'Undertale', COUNT(DISTINCT work_id) 
FROM undertale
	
SELECT 'Doctor Who', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN doctorWho 
ON works.work_id = doctorWho.work_id 

SELECT 'Hamilton', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN hamilton
ON works.work_id = hamilton.work_id 

SELECT 'Les Mis', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN lesMis
ON works.work_id = lesMis.work_id 

SELECT 'Sherlock', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN sherlock
ON works.work_id = sherlock.work_id 

SELECT 'Tolkien', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN tolkien 
ON works.work_id = tolkien.work_id 

SELECT 'Undertale', 
	COUNT(works.work_id), 
	SUM(words),
	MIN(words),
	MAX(words),
	ROUND(AVG(words))
FROM works
JOIN undertale
ON works.work_id = undertale.work_id 
