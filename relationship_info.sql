CREATE VIEW relationshipCount AS
	SELECT relationship
		, COUNT(*)
	FROM relationship
	GROUP BY relationship 
	ORDER BY COUNT(*) DESC

SELECT * FROM relationshipCount 

CREATE VIEW relationshipMarketBasket AS
	WITH cats (baseCat, affCat, Frequency) AS (
		SELECT
			p."relationship" AS baseCat,
			t."relationship" AS affCat,
			COUNT(*) AS Frequency
		FROM
			relationship p
		JOIN relationship t ON
			p.work_id = t.work_id
		WHERE
			p.relationship <> t.relationship 
		GROUP BY
			p.relationship ,
			t.relationship 
		HAVING COUNT(*) > 1 
	)
	SELECT
		b.baseCat "Base Relationship",
		b.catCount AS "# Works With Relationship",
		cats.affCat "Accompanied By Relationship",
		cats.Frequency "# Accompanied w/Base",
		CAST (cats.Frequency AS float)
				/CAST(b.catCount as float) "Affinity %"
	FROM cats 
	INNER JOIN
		(
			SELECT relationship baseCat, COUNT(*) AS catCount
			FROM relationship c 
			GROUP BY relationship 
			HAVING COUNT(*) >= 50
		) AS b
	ON b.baseCat = cats.baseCat
	ORDER BY 5 DESC 

SELECT * FROM relationshipMarketBasket 	

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN doctorWho 
ON doctorWho.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN hamilton 
ON hamilton.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN lesMis
ON lesMis.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN sherlock 
ON sherlock.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN tolkien 
ON tolkien.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN undertale 
ON undertale.work_id = c.work_id 
WHERE category = 'M/M'
GROUP BY relationship 
HAVING COUNT(*) >= 5
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN doctorWho 
ON doctorWho.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN hamilton 
ON hamilton.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN lesMis 
ON lesMis.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN sherlock 
ON sherlock.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN tolkien 
ON tolkien.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
JOIN undertale 
ON undertale.work_id = c.work_id 
WHERE category = 'F/M'
GROUP BY relationship 
HAVING COUNT(*) >= 5
ORDER BY COUNT(*) DESC

SELECT relationship, COUNT(*)
FROM relationship
JOIN category c
ON c.work_id = relationship.work_id
WHERE category = 'F/F'
GROUP BY relationship 
HAVING COUNT(*) >= 50
ORDER BY COUNT(*) DESC

