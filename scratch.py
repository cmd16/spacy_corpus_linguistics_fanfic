import spacy
from spacy.tokens import Doc, DocBin
import spacy_components
import os
import json
import time
from datetime import timedelta
import csv
import pandas as pd

stored_attrs = ['token', 'token_lower', 'word', 'bigram', 'trigram']
ents_list = ["PERSON", "NORP", "FAC", "ORG", "GPE", "LOC", "PRODUCT", "EVENT", "WORK_OF_ART", "LAW", "LANGUAGE"]
dep_list = ['root', 'acl', 'acomp', 'advcl', 'advmod', 'agent', 'amod', 'appos', 'attr', 'aux', 'auxpass',
                         'case', 'cc', 'ccomp', 'compound', 'conj', 'csubj', 'csubjpass', 'dative', 'dep', 'det', 'dobj',
                         'expl', 'mark', 'meta', 'neg', 'nmod', 'npadvmod', 'nsubj', 'nsubjpass', 'nummod', 'oprd',
                         'parataxis', 'pcomp', 'pobj', 'poss', 'preconj', 'predet', 'prep', 'prt', 'punct', 'quantmod',
                         'relcl', 'xcomp']
pos_list = ["ADJ", "NOUN", "PROPN", "PART", "ADV", "INTJ"]
tag_list = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]
other_attrs = ['noun_chunks', 'polarity', 'polarity_dist', 'word_lower']

# proj_dir = "/Volumes/2TB/Final_Project/Spacy"
proj_dir = "/Volumes/2TB/Final_Project/"

# with open(os.path.join(proj_dir, "works_text.csv"), "w", newline="") as csvfile:
#     writer = csv.writer(csvfile)
#     writer.writerow(["work_id", "text"])
#     i = 0
#     try:
#         for fname in os.listdir(os.path.join(proj_dir, "Fanfic_all")):
#             if not fname.endswith(".txt"):
#                 continue
#             if i % 1000 == 0:
#                 print(fname)
#             with open(os.path.join(os.path.join(proj_dir, "Fanfic_all"), fname), 'r') as f:
#                 text = f.read()
#                 writer.writerow([fname.replace(".txt", ""), text])
#             i += 1
#     except KeyboardInterrupt as e:
#         print("interrupted at", fname)
#         raise e

with open(os.path.join(proj_dir, "LIWC2015 Results (works_text.csv).csv"), 'r') as liwc_results:
    df = pd.read_csv()

# for my_list in [stored_attrs, ents_list, dep_list, pos_list, tag_list, other_attrs]:
#     for item in my_list:
#         os.mkdir(os.path.join('/Users/cat/reddit-corpus-linguistics/stats/keywords', f"{item.lower()}_fd"))
#
# nlp = spacy.load("en_core_web_sm")
# nlp.add_pipe(spacy_components.TokenFreqDistComponent(nlp), last=True)
# nlp.add_pipe(spacy_components.WordFreqDistComponent(nlp), last=True)
# nlp.add_pipe(spacy_components.BigramTrigramFreqDistComponent(nlp), last=True)
# nlp.add_pipe(spacy_components.VaderSentimentComponent(nlp), last=True)
#
# Doc.set_extension("filename", default=None)

# with open(os.path.join('/Users/cat/reddit-corpus-linguistics', 'fountainpens_comments_docbin'), 'rb') as f:
#     doc_bytes = f.read()
# doc_bin = DocBin(store_user_data=True).from_bytes(doc_bytes)
# data = []
# print('reading docs')
# start = time.time()
# for doc in doc_bin.get_docs(nlp.vocab):
#     result = {'id': doc._.filename}
#     for key in doc._.polarity_scores:
#         result[key] = doc._.polarity_scores[key]
#     data.append(result)
# print(f'finished reading docs, took {timedelta(seconds=time.time()-start)}. Now writing data')
# start = time.time()
# with open(os.path.join('/Users/cat/reddit-corpus-linguistics/stats/polarity', 'fountainpens_comments_all.txt'), 'w') as f:
#     f.write(json.dumps(data))
# print(f'wrote data, took {timedelta(seconds=time.time()-start)}')

