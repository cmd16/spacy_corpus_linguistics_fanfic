CREATE VIEW tagCount AS
	SELECT "additional tags"
		, COUNT(*)
	FROM additional_tags at2
	GROUP BY "additional tags"
	ORDER BY COUNT(*) DESC 

CREATE VIEW tagsMarketBasket AS
	WITH cats (baseCat, affCat, Frequency) AS (
		SELECT
			p."additional tags" AS baseCat,
			t."additional tags" AS affCat,
			COUNT(*) AS Frequency
		FROM
			additional_tags p
		JOIN additional_tags t ON
			p.work_id = t.work_id
		WHERE
			p."additional tags" <> t."additional tags" 
		GROUP BY
			p."additional tags" ,
			t."additional tags" 
		HAVING COUNT(*) > 1 
	)
	SELECT
		b.baseCat "Base Tag",
		b.catCount AS "# Works With Tag",
		cats.affCat "Accompanied By Tag",
		cats.Frequency "# Accompanied w/Base",
		CAST (cats.Frequency AS float)
				/CAST(b.catCount as float) "Affinity %"
	FROM cats 
	INNER JOIN
		(SELECT "additional tags" baseCat, COUNT(*) AS catCount
		FROM additional_tags c 
		GROUP BY "additional tags" 
		HAVING COUNT(*) >= 500) AS b
	ON b.baseCat = cats.baseCat
	ORDER BY 5 DESC 

SELECT fandom, COUNT(DISTINCT f2.work_id)
FROM fandom f2 
JOIN additional_tags at2 
ON f2.work_id = at2.work_id 
WHERE at2."additional tags" LIKE "Writing%"
GROUP BY fandom 
ORDER BY COUNT(*) DESC

SELECT fandom, COUNT(DISTINCT f2.work_id)
FROM fandom f2 
JOIN additional_tags at2 
ON f2.work_id = at2.work_id 
WHERE at2."additional tags" LIKE "Characters%"
GROUP BY fandom 
ORDER BY COUNT(*) DESC