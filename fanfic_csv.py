import os
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.types import String, Date, Integer

class FanficListManager:

    single_fields = ["work_id", "title", "rating", "language", "published", "status", "status date", "words", "chapters", "comments", "kudos", "bookmarks", "hits"]
    serial_fields = ["category", "fandom", "relationship", "character", "additional tags"]

    def __init__(self, in_name):
        self.in_name = in_name
        self.df = pd.read_csv(in_name, parse_dates=['published', 'status date']).drop(columns=['Unnamed: 0', 'body'])
        self.subset = None

    def to_sql(self, engine):
        self.df.drop(columns=self.serial_fields).to_sql('works', con=engine,
                       dtype={"work_id": String, "title": String, "rating": String(1), "language": String,
                              "published": Date, "status": String, "status date": Date, "words": Integer,
                              "comments": Integer, "kudos": Integer, "bookmarks": Integer, "hits": Integer,
                              }, if_exists="replace", index=False)
        self.serial_to_sql(engine)

    def serial_to_sql(self, engine):
        for field in self.serial_fields:
            field_df = self.df[['work_id', field]]
            field_df = field_df.assign(field=field_df[field].str.split(", ")).explode("field").drop(columns=[field]).rename(columns={"field": field})
            # field_df = field_df.assign(field=field_df["field"].str.strip())
            print(field)
            print(field_df.head())
            field_df.to_sql(field.replace(" ", "_"), con=engine,
                               dtype={"work_id": String, field: String
                                      }, if_exists="replace", index=False)


    @staticmethod
    def generate_serial_query(colname, include=None, exclude=None, equals=None):
        if equals:
            return f"{colname} == '{equals}'"
        if include:
            return f"{colname}.str.contains('{include}', na=False)"
        if exclude:
            return f"{colname}.str.contains('{exclude}', na=False)"

    @staticmethod
    def generate_wordcount_queries():
        range_tuples = [(1, 100), (1, 1000), (1001, 5000), (5001, 10000), (1001, 10000), (10001, 50000),
                        (50001, 100000),
                        (10001, 100000), (100001, 500000), (500001, 1000000), (100001, 1000000)]
        for rtuple in range_tuples:
            range_query = FanficListManager.generate_wordcount_query(rtuple)
            yield(rtuple, range_query)

    @staticmethod
    def generate_wordcount_query(rtuple):
        range_query = f"(words >= {rtuple[0]}) and (words <= {rtuple[1]})"
        return range_query

    @staticmethod
    def generate_year_queries():
        years = range(2009, 2019)
        for year in years:
            year_query = f"published.dt.year == {year}"
            yield year, year_query

    @staticmethod
    def generate_category_queries():
        categories = ("F/M", "M/M", "F/F", "Gen", "Multi", "Other")
        for category in categories:
            yield category, FanficListManager.generate_serial_query("category", include=category)

    @staticmethod
    def generate_status_queries():
        statuses = ("Completed", "Updated")
        for status in statuses:
            yield status, f"status == '{status}'"

    @staticmethod
    def generate_rating_queries():
        ratings = ("General Audiences", "Teen And Up Audiences", "Mature", "Explicit", "Not Rated")
        for rating in ratings:
            yield rating, f"rating == '{rating}'"

    @staticmethod
    def generate_tag_queries():
        tags = ("Fluff", "Alternate Universe", "Angst", "Hurt/Comfort", "Romance", "Friendship",
                "Humor", "Love", "Established", "Pining", ("Slow Build", "Slow Burn"), "Drama",
                "Domestic", "Friends to Lovers", "First", "Crack")
        for tag in tags:
            if type(tag) != str:
                query = fm.build_query(serial_queries=[{"colname": "`additional tags`", "include": t} for t in tag], use_or=True)
                tag_name = tag.join("+")
            else:
                query = fm.generate_serial_query(colname="`additional tags`", include=tag)
                tag_name = tag
            yield tag_name, query

    def build_query(self, **kwargs):
        queries = kwargs.get("queries", [])
        serial_queries = kwargs.get("serial_queries")
        for query in serial_queries:
            queries.append(self.generate_serial_query(colname=query.get("colname"), include=query.get("include"),
                                                      exclude=query.get("exclude"), equals=query.get("equals")))

        queries = [f"({x})" for x in queries]  # wrap statements in parentheses to avoid issues with precedence

        if kwargs.get("use_or"):
            joiner = " or "
        else:
            joiner = " and "
        return joiner.join(queries)

    def get_df_subset(self, **kwargs):
        query = kwargs.get("query")
        if query is None:
            query = self.build_query(**kwargs)
        self.subset = self.df.query(query)

    def get_subset_ids(self, **kwargs):
        if not self.subset or kwargs.get("new_subset"):
            self.get_df_subset(**kwargs)
        return self.subset["work_id"]

    def write_subset_ids(self, outname, **kwargs):
        subset_ids = self.get_subset_ids(**kwargs)
        if not subset_ids:  # if no results, don't make a file
            return
        with open(outname) as f_out:
            for id in subset_ids:
                f_out.write(f"{id}\n")

if __name__ == "__main__":
    proj_dir = "/Volumes/2TB/Final_Project"
    in_name = os.path.join(proj_dir, "csv_info/CSV/all_fanfic.csv")
    out_dir = os.path.join(proj_dir, "Fanfic_lists")

    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 1000)

    fm = FanficListManager(in_name=in_name)

    fm.to_sql('sqlite:///fanfic_info.db')

    # print(fm.df.info())
    # # print(fm.df.iloc[0])
    print(fm.df.columns.values)
    # df_9 = fm.df.query("work_id.astype('str') > '90'", engine="python")
    # print(len(df_9))
    # print(df_9['words'].sum())
    # df_8 = fm.df.query("'80' < work_id.astype('str') < '90'", engine="python")
    # print(len(df_8))
    # print(df_8['words'].sum())

    # tags = ["Alternate", "Alternate Universe"]
    # for tag in tags:
    #     if type(tag) != str:
    #         query = fm.build_query(serial_queries=[{"colname": "`additional tags`", "include": t} for t in tag], use_or=True)
    #     else:
    #         query = fm.generate_serial_query(colname="`additional tags`", include=tag)
    #     try:
    #         fd = fm.df.query(query, engine="python")
    #     except Exception as e:
    #         print("failed query:", query)
    #         raise e
    #     print(query, len(fd))

    fandoms = {"Doctor Who": ("Doctor Who", "Torchwood", "Sarah Jane"), "Hamilton": "Hamilton - Miranda", "Les Mis": "Les Miserables",
               "Sherlock": ("Sherlock", "Elementary"), "Star Trek": {"Star Trek"},
               "Tolkien": ("Tolkien", "Lord of the Rings", "Hobbit"), "Undertale": ("Undertale", "Deltarune")}

    # for fandom in fandoms:
    #     print(fandom)
    #     fandom_query = " or ".join(f"fandom.str.contains('{x}')" for x in fandoms[fandom])
    #     fm.write_subset_ids(outname=f"{os.path.join(out_dir, fandom)}.txt", new_subset=True, queries=[fandom_query])
    #
    #     for rtuple, wordcount_query in fm.generate_wordcount_queries():
    #         print(fandom, rtuple)
    #         range_query = f"(words >= {rtuple[0]}) and (words <= {rtuple[1]})"
    #         outname = os.path.join(out_dir, f"{fandom}_words_{rtuple[0]}-{rtuple[1]}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, wordcount_query])
    #
    #     for year, year_query in fm.generate_year_queries():
    #         print(fandom, year)
    #         outname = os.path.join(out_dir, f"{fandom}_year_{year}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, year_query])
    #
    #     for category, category_query in fm.generate_category_queries():
    #         print(fandom, category)
    #         outname = os.path.join(out_dir, f"{fandom}_category_{category}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, category_query])
    #
    #     for status, status_query in fm.generate_status_queries():
    #         print(fandom, status)
    #         outname = os.path.join(out_dir, f"{fandom}_status_{status}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, status_query])
    #
    #     for rating, rating_query in fm.generate_rating_queries():
    #         print(fandom, rating)
    #         outname = os.path.join(out_dir, f"{fandom}_rating_{rating}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, rating_query])
    #
    #     for tag, tag_query in fm.generate_tag_queries():
    #         print(fandom, tag)
    #         outname = os.path.join(out_dir, f"{fandom}_tag_{tag}.txt")
    #         fm.write_subset_ids(outname=outname, new_subset=True, queries=[fandom_query, tag_query])
